const config  = require('../config/config');

import { Router, Request, Response, NextFunction } from 'express';
const AssetsProd = require('../gew2');
import { connect } from 'mongoose';
import passport = require('passport');
import { sign, verify, TokenExpiredError } from 'jsonwebtoken';
import ContractPaymentController from '../controllers/ContractPaymentController';
const requireAuth = passport.authenticate('jwt', { session: false });  

export class ContractPaymentRouter {

  router: Router;
  authRoutes: Router;
  contractPaymentController: ContractPaymentController = new ContractPaymentController();
  
  constructor() {
    this.router = Router();
    this.init();
  }
  
  init() {

    this.router.post('/', requireAuth, this.contractPaymentController.save);
    this.router.put('/:id', requireAuth ,this.contractPaymentController.update);
    this.router.get('/' , requireAuth ,this.contractPaymentController.getAll);
    this.router.get('/:id', requireAuth ,this.contractPaymentController.getOne);
    this.router.delete('/:id', requireAuth, this.contractPaymentController.delete);

  }
}

// Create the productRouter, and export its configured Express.Router
const contractPaymentRouter = new ContractPaymentRouter();
contractPaymentRouter.init();

export default contractPaymentRouter.router;