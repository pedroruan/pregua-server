import { Router, Request, Response, NextFunction } from 'express';
const config = require('../config/config');
import { connect } from 'mongoose';
import { ContractPayment } from '../models/contract-payment';

export default class ContractPaymentController {

    constructor() { }

    public save(req: Request, res: Response, next: NextFunction) {
        const contract = new ContractPayment();
        contract.name = req.body.name;
        contract.methods = req.body.methods;
        contract.idEmpresa = req.user.idEmpresa;
        contract.save()
            .then((created: any) => {
                res.json({ message: 'Contract Payment created! Id = ' + created.id, created: created });
            })
            .catch((err) => {
                res.status(500).send(err);
            });

    }

    public update(req: Request, res: Response, next: NextFunction) {
        const contract = new ContractPayment();
        contract.findById(req.params.id, function (err, product) {
            if (err)
                res.send(err);

            contract.name = req.body.name;
            contract.methods = req.body.methods;
            contract.save()
                .then((updated: any) => {
                    res.json({ message: 'Contract Payment updated! Id = ' + updated.id, updated: updated });
                })
                .catch((err) => {
                    res.status(500).send(err);
                });

        });
    }

    public delete(req: Request, res: Response, next: NextFunction) {
        ContractPayment.remove({
            _id: req.params.id
        }, function (err, project) {
            if (err) {
                console.error(`${req.params.id} error on delete`);
                res.status(500).send(err);
            }
            res.json({ message: 'Successfully deleted' });
        });
    }

    public getAll(req: Request, res: Response, next: NextFunction) {
        let query = { "idEmpresa": req.user.idEmpresa };
        ContractPayment.find(query).exec((err, contracts) => {
            if (err)
                res.status(500).json(err);
            res.json(contracts);
        });
    }

    public validar(req: Request, res: Response, next: NextFunction) {
        const name = req.body.name;
        if (!req.body.methods || req.body.methods.length == 0) {
            return res.status(422).json({ message: 'Precisa de pelo menos um método adicionado.' });
        }
        if (!name) {
            return res.status(422).json({ message: 'É necessário identificar o contrato com um nome.' });
        }
    }

    public getOne(req, res, next) {
        let query = req.params.id;

        ContractPayment.findOne({ "_id": query }).exec(function (err, contract) {
            if (contract) {
                return res.status(200).json(contract);
            } else {
                return res.status(404)
                    .json({
                        message: 'Contrato não encontrado',
                        status: res.status
                    });
            }
        })
    }


}

