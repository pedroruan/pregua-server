var mongoose = require('mongoose');
var Schema = mongoose.Schema;
import { Document, Schema, Model, model } from 'mongoose';

export const PaymentMethodSchema: Schema = new Schema({
    //Credit, debit, alimentacao, refeicao
    method:String,
    //visa, master, dinners
    brands:[String],
    discountValue:Number,
    daysToReturn:Number,
    bussinessDay:Boolean
});

export const ContractPaymentSchema: Schema = new Schema({
    name: String,
    methods:[PaymentMethodSchema],
    idEmpresa:String
});

export const ContractPayment: Model = model("ContractPayment", ContractPaymentSchema);