export interface FechamentoModel {
    valorDebito: number;
    valorCredito: number;
    valorDinheiro: number;
    qtdDebito: number;
    qtdCredito: number;
    qtdDinheiro: number;
    valorMedio: number;    
    valorTotal: number;
    fechamentoCaixaId:string;
    produtosVendidos:any;
}